$(document).ready(function() {
  var $form = $('#formspree-form');
  var $field_subject = $form.find('input[name="subject"]');
  var $field_email = $form.find('input[name="email"]');
  var $field_message = $form.find('textarea[name="message"]');
  var $form_submit = $form.find('button[type="submit"]');

  $form_submit.on({
    'click': function(e) {
      var email_val = $field_email.val();
      var subject_val = $field_subject.val();
      var message_val = $field_message.val();

      e.preventDefault(); // stop default form submit

      /* validation logic goes here */

      $.ajax({
        url: "//formspree.io/your@email.com",
        method: "POST",
        data: {
          "email": email_val,
          "subject": subject_val,
          "message": message_val
        },
        dataType: "json",
        beforeSend: function() {
          console.log("Sending message.");
        },
        success: function() {
          $form[0].reset(); // reset form fields
          console.log("Message submitted.");
        },
        fail: function() {
          console.log("Message failed.");
        }
      });

    }
  });
});
